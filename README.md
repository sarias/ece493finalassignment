# Slime Volleyball Gym Environment - Assignment 3

By Steve Arias and Aditya Sharma

## Domains for this Assignment
This assignment will use two domains to test out Deep RL algorithms. 
1. `Maze World` from Assignment 2 (See https://git.uwaterloo.ca/mcrowley/ece493t25-assignment2-spring2020)
2. `SlimeVolleyGym`: this is a simple gym environment for testing Reinforcement Learning algorithms. Refer to the original repo at [slimevolleygym](https://github.com/hardmaru/slimevolleygym) to get more information about this environment. 

## How We Implemented Maze World
We used the gym docs on creating a custom environment (https://github.com/openai/gym/blob/master/docs/creating-environments.md) as our primary resource. We used the main components of `maze_env.py` from Assignment 2 and changed a few things on it. `MazeWorldEnv.py` is our custom Maze World environment. The main change we did was adding an action and observation space (https://medium.com/swlh/states-observation-and-action-spaces-in-reinforcement-learning-569a30a8d2a1) to our model. We used an action space of `spaces.Discrete(4)` since the agent can do 4 possible actions (left, right, up, down). Then, we set the observation space to `spaces.Box(low=5.0, high=395.0, shape=(4,))`. In assignment 2, `canvas` was used to develop a graphic of Maze World. The states in `canvas` in `MazeWorldEnv.py` is graphic locations ([5.0, 5.0, 35.0, 35.0] for the most left top box for example). The shape of the observation space is a 4-element tuple representing the bounding box of the agent in the graphic.

## How to Train and Run Models for Slime Volleyball Gym
To train the DQN model, run the following in the command line:
```
python3 training_scripts/train_dqn.py
```
This will create a folder called `dqn` which will have a `final_model.zip`. Once the training is done, move this folder to the `zoo` folder. Then, to run the model, run the following in the command line:
```
python3 eval_dqn.py
```
This will produce a graph that shows the reward of the model over multiple episodes. Repeat this for A2C and PPO (replace `dqn` in the steps mentioned above with either `a2c` or `ppo` depending on what model you want).

## How to Train and Run Models for Maze World
To train the DQN model, run the following in the command line:
```
python3 training_scripts/train_dqn_maze.py
```
This will create a folder called `dqn_mazeworld` which will have a `final_model.zip`. Once the training is done, move this folder to the `zoo` folder. Then, to run the model, run the following in the command line:
```
python3 eval_dqn_maze.py
```
This will produce a graph that shows the reward of the model over multiple episodes. Repeat this for A2C and PPO (replace `dqn` in the steps mentioned above with either `a2c` or `ppo` depending on what model you want).