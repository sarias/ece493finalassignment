"""
Simple evaluation example.

run: python eval_dqn_maze.py --render

Evaluate DQN policy in MazeWorld

"""

import warnings
# numpy warnings because of tensorflow
warnings.filterwarnings("ignore", category=FutureWarning, module='tensorflow')
warnings.filterwarnings("ignore", category=UserWarning, module='gym')

import gym
import numpy as np
import argparse
import matplotlib.pyplot as plt
from training_scripts.MazeWorldEnv import MazeWorldEnv

import slimevolleygym
from stable_baselines.common.policies import MlpPolicy
from stable_baselines import DQN

def rollout(env, policy, render_mode=False):
  obs = env.reset()

  done = False
  total_reward = 0
  while not done:
    action, _states = policy.predict(obs, deterministic=True)
    obs, reward, done, _ = env.step(action)
    total_reward += reward

    if render_mode:
      env.render()

  return total_reward

if __name__=="__main__":

  parser = argparse.ArgumentParser(description='Evaluate pre-trained DQN agent.')
  parser.add_argument('--model-path', help='path to stable-baselines model.',
                        type=str, default="zoo/dqn_mazeworld/final_model.zip")
  parser.add_argument('--render', action='store_true', help='render to screen?', default=False)

  args = parser.parse_args()
  render_mode = args.render

  env = MazeWorldEnv()

  print("Loading", args.model_path)
  policy = DQN.load(args.model_path, env=env)
  history = []
  for i in range(1000):
    env.seed(seed=i)
    cumulative_score = rollout(env, policy, render_mode)
    print("cumulative score #", i, ":", cumulative_score)
    history.append(cumulative_score)

  print("history dump:", history)
  print("average score", np.mean(history), "standard_deviation", np.std(history))
  plt.plot(history)
  plt.title("DQN Policy In MazeWorld")
  plt.xlabel('Experiment Round')
  plt.ylabel('Reward')
  plt.savefig("eval_dqn_maze_results.png")
  plt.show()
