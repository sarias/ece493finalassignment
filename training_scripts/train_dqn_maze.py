#!/usr/bin/env python3

# Train single CPU DQN on slimevolley.
# Should solve it (beat existing AI on average over 1000 trials) in 3 hours on single CPU, within 3M steps.

import os
import gym
import slimevolleygym
from slimevolleygym import SurvivalRewardEnv
from MazeWorldEnv import MazeWorldEnv

from stable_baselines import DQN
from stable_baselines.deepq.policies import MlpPolicy, CnnPolicy
from stable_baselines import logger
from stable_baselines.common.callbacks import EvalCallback
from stable_baselines.common.env_checker import check_env

NUM_TIMESTEPS = int(2e5)
SEED = 721
EVAL_FREQ = 25000
EVAL_EPISODES = 1000
LOGDIR = "dqn_mazeworld" # moved to zoo afterwards.

logger.configure(folder=LOGDIR)

env = MazeWorldEnv()
# If the environment don't follow the interface, an error will be thrown
check_env(env, warn=True)
env.seed(SEED)
model = DQN(MlpPolicy, env, verbose=1)
model.learn(total_timesteps=NUM_TIMESTEPS)

model.save(os.path.join(LOGDIR, "final_model"))

env.close()
