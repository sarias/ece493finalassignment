#!/usr/bin/env python3

# Train single CPU A2C on slimevolley.
# Should solve it (beat existing AI on average over 1000 trials) in 3 hours on single CPU, within 3M steps.

import os
import gym
import slimevolleygym
from slimevolleygym import SurvivalRewardEnv

from stable_baselines import A2C
from stable_baselines.common.policies import MlpPolicy
from stable_baselines import logger
from stable_baselines.common.callbacks import EvalCallback
from stable_baselines.common import make_vec_env

# 10,000,000
NUM_TIMESTEPS = 10000000
SEED = 721
EVAL_FREQ = 250000
EVAL_EPISODES = 1000
LOGDIR = "a2c" # moved to zoo afterwards.

logger.configure(folder=LOGDIR)

# env = gym.make("SlimeVolley-v0")
env = make_vec_env('SlimeVolley-v0', n_envs=4)
env.seed(SEED)

model = A2C(MlpPolicy, env, verbose=1)
#model.learn(total_timesteps=25000)
# eval_callback = EvalCallback(env, best_model_save_path=LOGDIR, log_path=LOGDIR, eval_freq=EVAL_FREQ, n_eval_episodes=EVAL_EPISODES)
# model.learn(total_timesteps=NUM_TIMESTEPS, callback=eval_callback)
model.learn(total_timesteps=NUM_TIMESTEPS)

model.save(os.path.join(LOGDIR, "final_model"))

env.close()
